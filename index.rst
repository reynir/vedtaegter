.. Data Coop Documentation documentation master file, created by
   sphinx-quickstart on Sat Nov  7 12:15:08 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Vedtægter for datafællesskabet data.coop
========================================

.. toctree::
   :maxdepth: 2

   vedtægter



* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

